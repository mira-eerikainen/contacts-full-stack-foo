## Local testing

## Installing packages at backend (cf. below for details)
Remember to run:
npm i 

### Creating db instances (if they don't exist)
docker run --name my-postgres -e POSTGRES_PASSWORD="POSTGRES_PASSWORD" -e POSTGRES_USER=pguser -e POSTGRES_DB=mydb -p 5432:5432 -d postgres:13.5
docker run --name my-pgadmin -p 5050:80 -e "PGADMIN_DEFAULT_EMAIL="email"" -e "PGADMIN_DEFAULT_PASSWORD="password"" -d dpage/pgadmin4

### Monitoring :
login to database using browser localhost:5050
docker inspect my-postgres | grep IPAddress

### Restarting db instances
docker ps -a
docker start my-postgres
docker start my-pgadmin

### Create .env with following parameters (in the /backend folder)
### Note : suitable db must be created in my-pgadmin
APP_PG_USER=

APP_PG_HOST=

APP_PG_DB=

APP_PG_PASSWORD=

APP_PG_PORT=

APP_PORT=

###  Testing database (at backend)
npm run testdb

### Create a sample testing database
npm run createdb


## Installed packages

###  Express (basis)
npm i --save express
npm i --save express-async-errors

### PostgreSQL client for Node.js
npm i --save pg

### UUIDv4 (for unique identifiers)
npm i --save uuid

### For security
npm i --save helmet

### For user authentication
npm i --save bcrypt
npm i --save jsonwebtoken

### Support for modules:
package.json:
type: modules

### Eslint (for beautiful code)
npm i --save-dev eslint
npm i --save-dev eslint-plugin-jest

npx eslint --init
(indent : 2, linebreak-style : off)

Modify in .eslintrc.cjs:
  "plugins": ["jest"],
  "extends": ["plugin:jest/all","eslint:recommended"],
  "ignorePatterns": ["/test/*"],
  "jest/no-disabled-tests": "warn",
  "jest/no-focused-tests": "error",
  "jest/no-identical-title": "error",
  "jest/prefer-to-have-length": "warn",
  "jest/valid-expect": "error",
  "jest/require-hook": 0,

### Environment variables:
npm i --save dotenv 
#### for Windows
npm i --save-dev cross-env


### Testing
npm i --save-dev jest
npm i --save-dev jest-mock
npm i --save-dev nodemon
npm i --save-dev supertest

### Setting at package.json: 
  "jest": {
    "transform": {},
    "collectCoverage": true,
    "testEnvironment": "jest-environment-node"
  }

### Scripts
added to package.json
    "dev": "cross-env NODE_ENV='dev' nodemon src/index.js",
    "test": "cross-env NODE_ENV='test' NODE_OPTIONS='--experimental-vm-modules' jest --verbose",
    "start": "cross-env NODE_ENV='prod' node src/index.js"
    etc.
    

