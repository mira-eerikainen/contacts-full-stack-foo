import {jest} from "@jest/globals";
import request from "supertest";
import app from "../../src/index.js";
import { pool } from "../../src/db/db.js";

const initializeDbMock = (expectedResponse) => {
  jest.spyOn(pool, "connect").mockImplementation(() => {
    return {
      query: () => expectedResponse,
      release: () => null
    };
  });
};

describe("testing contact API", () => {
  it("root responds OK", async () => {
    expect.assertions(1);
    const response = await request(app).get("/");
    expect(response.status).toBe(200);
  });
});

describe("testing GET /contacts", () => {

  const mockResponse = { 
    rows: [
      {"id" : 0, "firstname": "Teppo", "lastname": "Testaaja", "email": "teppo@testaaja.fi","phone": "+358400123123", "address": "Mannerheimintie 1, 00100 Helsinki","notes": ""},
      {"id" : 1, "firstname": "Matti", "lastname": "Mainio", "email":  "matti@mainiola.fi","phone": "+35812312323123", "address": "Mannerheimintie 2B, 00100 Helsinki","notes": ""},
      {"id" : 2, "firstname": "Aku", "lastname" : "Ankka", "email" : "aku@ankka.fi","phone": "+35812312323123", "address": "Mannerheimintie 3C, 00150 Helsinki", "notes": ""}
    ] 
  };

  beforeEach( () => {
    initializeDbMock(mockResponse);
  });

  afterEach(() => {
    jest.clearAllMocks();
  });

  it("returns 200 with all the contacts", async () => {
    expect.assertions(2);
    const response = await request(app)
      .get("/contacts")
      .set("Content-Type", "application/json");
    expect(response.status).toBe(200);
    expect(JSON.parse(response.text)).toStrictEqual(mockResponse.rows);
  });

});

describe("testing GET /contacts/id", () => {

  const mockResponse = { 
    rowCount: 1,
    rows: [
      {"id" : 1, "firstname": "Matti", "lastname": "Mainio", "email":  "matti@mainiola.fi","phone": "+35812312323123", "address": "Mannerheimintie 2B, 00100 Helsinki","notes": ""},
    ] 
  };
  
  beforeEach( () => {
    initializeDbMock(mockResponse);
  });
  
  afterEach(() => {
    jest.clearAllMocks();
  });
  
  it("returns 200 with a single contact", async () => {
    expect.assertions(2);
    const response = await request(app)
      .get("/contacts/1")
      .set("Content-Type", "application/json");
    expect(response.status).toBe(200);
    expect(JSON.parse(response.text)).toStrictEqual(mockResponse.rows[0]);
  });
  

});

describe("testing GET /contacts/id, non-FOUND contact", () => {
  
  // Expect not found
  it("returns 404 with a non-found contact", async () => {
    expect.assertions(1);
    const mockResponse = {
      rowCount: 0,
      rows: [] 
    };
    initializeDbMock(mockResponse);
    const response = await request(app).get("/contacts/4");
    expect(response.status).toBe(404);        
  });   
});


describe("testing POST /contacts/", () => {
   
  it("posting a new contact", async () => {
    expect.assertions(2);
    const response = await request(app).post("/contacts/")
      .send ({
        "id" : "15",
        "firstname" : "Musta",
        "lastname" : "Naamio",
        "email" : "musta@naamio.fi",
        "address" : "Mannerheimintie 8 J, Helsinki",
        "notes" : ""
      } );
    expect(response.status).toBe(201);    
    expect(response.text).toBe("Created");   
  }); 

  it("posting a new contact with only two required fields", async () => {
    expect.assertions(2);
    const response = await request(app).post("/contacts/")
      .send ({
        "id" : "15",
        "firstname" : "Musta",
        "lastname" : "Naamio",
      } );
    expect(response.status).toBe(201);    
    expect(response.text).toBe("Created");   
  }); 

  it("posting a new contact with insufficient fields", async () => {
    expect.assertions(1);
    const response = await request(app).post("/contacts/")
      .send ({
        "firstname" : "Musta",
      } );
    expect(response.status).toBe(400);    
  }); 
       

});

describe("testing PUT /contacts/id", () => {
   
  it("updating a new contact", async () => {
    expect.assertions(2);
    const mockResponse = {
      rowCount: 1,
      rows: [] 
    };
    initializeDbMock(mockResponse);
    const response = await request(app).put("/contacts/0")
      .send ({
        "id" : "0",
        "firstname" : "Musta",
        "lastname" : "Naamio",
        "email" : "musta@naamio.fi",
        "address" : "Mannerheimintie 8 J, Helsinki",
        "notes" : ""
      } );
    expect(response.status).toBe(200);    
    expect(response.text).toBe("Contact updated succesfully.");   
  }); 
  
  it("updating contact with only two required fields", async () => {
    expect.assertions(2);
    const mockResponse = {
      rowCount: 1,
      rows: [] 
    };
    initializeDbMock(mockResponse);
    const response = await request(app).put("/contacts/1")
      .send ({
        "id" : "15",
        "firstname" : "Musta",
        "lastname" : "Naamio",
      } );
    expect(response.status).toBe(200);    
    expect(response.text).toBe("Contact updated succesfully.");   
  }); 
  
  it("updating a contact with insufficient fields", async () => {
    expect.assertions(1);
    const response = await request(app).put("/contacts/0")
      .send ({
        "firstname" : "Musta",
      } );
    expect(response.status).toBe(400);    
  }); 
         
  
});

describe("testing DELETE /contacts/:id", () => {

  it("deleting an existing contact, happy case", async () => {
    expect.assertions(1);
    const mockResponse = {
      rowCount: 1,
      rows: [] // ignored
    };
    initializeDbMock(mockResponse);
    const response = await request(app).delete("/contacts/1");
    expect(response.status).toBe(200);      
  });   

  it("trying to delete non-existing contact", async () => {
    expect.assertions(1);
    const mockResponse = {
      rowCount: 0,
      rows: [] // ignored
    };
    initializeDbMock(mockResponse);
    const response = await request(app).delete("/contacts/15");
    expect(response.status).toBe(404);      
  });  

});

