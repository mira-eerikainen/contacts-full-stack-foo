/* eslint-disable jest/require-hook */
// Create a default database with some values
import db from "../src/db/db.js";


let result = await db.createTables();

const initContactsTable = `
INSERT INTO contacts (id,firstname,lastname,email,phone,address,notes)
VALUES 
(0, 'Teppo', 'Testaaja', 'teppo@testaaja.fi','+358400123123', 'Mannerheimintie 1, 00100 Helsinki',''),
(1, 'Matti', 'Mainio', 'matti@mainiola.fi','+35812312323123', 'Mannerheimintie 2B, 00100 Helsinki',''),
(2, 'Aku', 'Ankka', 'aku@ankka.fi','+35812312323123', 'Mannerheimintie 3C, 00150 Helsinki','')
`;
result = await db.executeQuery(initContactsTable);
console.log(`Created a default database with ${result.rowCount} entries.`);
