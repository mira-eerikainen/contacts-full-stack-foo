/* eslint-disable jest/require-hook */
// test database connections
// this is not based on JEST
import contactDao from "../src/dao/contactDao.js";
import db from "../src/db/db.js";
let result;

// Debugging:
console.log("Testing database, phase I : Trying to Establish database");
await db.createTables();
console.log("");

console.log("Testing database, phase II : Emptying database");
console.log("   Deleting all contacts");
result = await db.executeQuery(
  "DELETE FROM " + db.tableName 
);
console.log("");

console.log("Testing database, phase III : Creating a template database");
const initContactsTable = `
INSERT INTO contacts (id,firstname,lastname,email,phone,address,notes)
VALUES 
(25, 'Teppo', 'Testaaja', 'teppo@testaaja.fi','+358400123123', 'Mannerheimintie 1, 00100 Helsinki',''),
(35, 'Matti', 'Mainio', 'matti@mainiola.fi','+35812312323123', 'Mannerheimintie 2B, 00100 Helsinki','')
`;
console.log("   Initialized 2 contact");
result = await db.executeQuery(initContactsTable);
console.log(`   Confirmed : ${result.rowCount}`);
console.log("");

console.log("Testing database, phase IV : Searching for results");
result = await contactDao.findAll();
console.log(`Expecting 2 contacts, found ${result.rowCount} contacts.`);
console.log("");

console.log("Testing database, phase V, a : Inserting a contact in number order");
const contact = 
{"id":"40","firstname":"Aku","lastname":"Ankka","email":"aku@ankka.fi","phone": "+35840123123", 
  "address":"Mannerheimintie 3B, 00100 Helsinki", "notes":""};
result = await contactDao.insertContact(contact);

console.log("Testing database, phase V, b : Inserting a contact in number order");
const contactB = 
{"id":"15","firstname":"Teppo","email":"teppo@tulppu.fi","lastname":"Tulppu", "phone": "+5123823123", 
  "address":"Mannerheimintie 4C, 00100 Helsinki", "notes":"moi moi"};
result = await contactDao.insertContact(contactB);
console.log("");

console.log("Testing database, phase VI : Find by ID");
console.log("Searching for entry with ID=25");
result = await contactDao.findByID(25);
console.log(`Results : ${result.rowCount} entries found.`);
console.log("Searching for entry with ID=35");
result = await contactDao.findByID(35);
console.log(`Results : ${result.rowCount} entries found.`);
console.log("");

console.log("Testing database, phase VII : Deleting by ID");
console.log("Deleting entry with ID=1");
result = await contactDao.deleteByID(1);
result = await contactDao.findAll();
console.log("Current table is :");
console.table(result.rows);
console.log("");

console.log("Testing database, phase VIII : Update by ID");
console.log("Updating entry with ID=0");
const contactC = 
{"id":25,"firstname":"Hannu","email":"hannu@hanhi.fi","lastname":"Hanhi", "phone": "+512123233123", 
  "address":"Mannerheimintie 5D, 00100 Helsinki", "notes":""};
result = await contactDao.updateByID(25, contactC);
console.log(`result.rowcount = ${result.rowCount}.`);
result = await contactDao.findAll();
console.log("Current table is :");
console.table(result.rows);
console.log("");