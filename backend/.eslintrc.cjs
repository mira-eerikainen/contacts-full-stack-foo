module.exports = {
  "env": {
    "es2021": true,
    "node": true,
    "jest/globals": true
  },
  "plugins": ["jest"],
  "extends": ["plugin:jest/all","eslint:recommended"],
  "parserOptions": {
    "ecmaVersion": "latest",
    "sourceType": "module"
  },
  "rules": {
    "indent": [
      "error",
      2
    ],
    "linebreak-style": [
      "off",
    ],
    "quotes": [
      "error",
      "double"
    ],
    "semi": [
      "error",
      "always"
    ],
    "jest/no-hooks": [
      "off"
    ]
  }
};
