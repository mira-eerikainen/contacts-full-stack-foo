/* eslint-disable jest/require-hook */
import express from "express";
import db from "./db/db.js";

// CRUD implemented:
// GET /contacts
// GET /contacts/id
// POST /contacts
// PUT /contacts/id
// DELETE /contacts/id


// User authentication
// cf. api/userRouter.js
// POST /user/create
// POST /user/login

// Classified information:
// GET /protected

// using .env for environment variables
import dotenv from "dotenv";
// for security
import helmet from "helmet";

// for error handling
import "express-async-errors";

import rootRouter from "./api/rootRouter.js";
import contactRouter from "./api/contactRouter.js";
import userRouter from "./api/userRouter.js";
import protectedRouter from "./api/protectedRouter.js";


import {
  unknownEndpoint,
  errorHandler,
  allowCors
} from "./middlewares.js";


const isProd = process.env.NODE_ENV === "prod";
const APP_PORT = process.env.APP_PORT || 8080;

const app = express();
!isProd && dotenv.config() && app.use(allowCors);

// enable all helmet security policies, refer to https://github.com/helmetjs/helmet
// details on effects : https://blog.logrocket.com/express-middleware-a-complete-guide/#helmet
// e.g. removed of X-Powered-By from headers
isProd && app.use(helmet());

app.use(express.json()); // this must be used to parse POST request body made in JSON format
app.use(express.static("build"));
app.use("/", rootRouter);
// everything under /contacts
app.use("/contacts", contactRouter);
app.use("/user", userRouter);
app.use("/protected", protectedRouter);
app.use(unknownEndpoint);
app.use(errorHandler);

// Create contacts table
process.env.NODE_ENV !== "test" && db.createTables();

(process.env.NODE_ENV !== "test") && app.listen(APP_PORT, () => {
  console.log(`Listening to port ${APP_PORT}`);
});

// must export the Express function => otherwise timeout will occur
export default app;