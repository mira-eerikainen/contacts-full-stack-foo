/* eslint-disable jest/require-hook */
import { Router } from "express";
import { authorize } from "../middlewares.js";

const router = Router();

router.use(authorize);

// GET /protected
router.get("/", async (req, res) => {
  res.status(200).send("Hello from protected router!");
});

export default router;