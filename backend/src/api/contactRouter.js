/* eslint-disable jest/require-hook */
import { Router } from "express";
import contactService from "../services/contactService.js";

const router = Router();

// everything under /contacts
router.get("/", async (_req, res) => {
  const contacts = await contactService.findAll();
  res.status(200).json(contacts);
});

// everything under /contacts
router.get("/:id", async (req, res) => {
  const reqID = req.params.id;
  const contact = await contactService.findByID(reqID);
  if (contact.rowCount === 0 ) {
    res.status(404).send(`No contact found with ID=${reqID}.`);
  }   
  else if (contact.rowCount === 1 ) {
    res.status(200).send(contact.rows[0]);
  }
});


// everything under /contacts
router.post("/", async (req, res) => {
  const contact = req.body;
  const result = await contactService.insertContact(contact);
  res.status(result ? 201 : 500).send(result ? "Created" : "Error");
  return;
});

// everything under /contacts
router.put("/:id", async (req, res) => {
  const contact = req.body;
  const reqID = req.params.id;
  const result = await contactService.updateByID(reqID,contact);

  // Debugging
  // console.log(`Put /contacts/${reqID}, result is :`);
  // console.table(result);
  // console.table(`rowCount = ${result.rowCount}`);

  // error 400 sent if parameters incomplete
  if (result.rowCount === 0) {
    res.status(404).send(`Didn't find contact with ID=${reqID}.`);
  } else {
    res.status(200).send("Contact updated succesfully.");
  }
    
});

router.delete("/:id", async (req, res) => {
  const reqID = req.params.id;
  const result = await contactService.deleteByID(reqID);
  // console.log(`At router : result is ${result}.`);
  if (result.rowCount === 0) {
    res.status(404).send(`Didn't find contact with ID=${reqID}.`);
  } else {
    res.status(200).send("Contact deleted succesfully.");
  }
});

export default router;