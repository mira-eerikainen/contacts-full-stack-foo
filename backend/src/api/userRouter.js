/* eslint-disable jest/require-hook */
import { Router } from "express";
import { createUser, login } from "../services/userService.js";

const router = Router();

// POST /user/create
// {
//    "username": XXX
//    "password": XXX
// }
router.post("/create", async (req, res) => {
  const result = await createUser(req.body);
  res.status(result ? 201 : 400).send();
});

// POST /user/login
// {
//    "username": XXX
//    "password": XXX
// }
// Returns Token in headers

router.post("/login", async (req, res) => {
  const token = await login(req.body);
  // Returns correct token in Headers

  res.status(200)
    .header("Token", token)
    .send();
});

export default router;