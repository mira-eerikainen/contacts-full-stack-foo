// Refer to : https://www.postgresql.org/docs/13/datatype.html
// version 13

const createContactsTable = `
    CREATE TABLE IF NOT EXISTS "contacts" (
	    "id" VARCHAR(100) NOT NULL,
	    "firstname" VARCHAR(100) NOT NULL,
	    "lastname" VARCHAR(100) NOT NULL,
        "email" VARCHAR(100),
        "phone" VARCHAR(100),
        "address" VARCHAR(100),
        "notes" VARCHAR(100),
	    PRIMARY KEY ("id")
    );`;

const createCustomerTable = `
    CREATE TABLE IF NOT EXISTS "customer" (
	    "id" VARCHAR(36) NOT NULL UNIQUE,
	    "username" VARCHAR(100) NOT NULL UNIQUE,
	    "passhash" VARCHAR(100) NOT NULL,
	    PRIMARY KEY ("id")
    );`;
  

export default { 
  createContactsTable, createCustomerTable
};