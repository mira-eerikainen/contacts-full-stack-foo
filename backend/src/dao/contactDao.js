// Refer to : https://www.postgresql.org/docs/13/datatype.html
// version 13
import db from "../db/db.js";


const findAll = async () => {

  // Debugging
  console.log("Requesting for all contacts...");
  const result = await db.executeQuery(
    "SELECT * FROM \"contacts\" ORDER BY \"firstname\" ASC;"
  );

  // Debugging
  console.log(`Found ${result.rowCount} contacts.`);
  return result;
};

// Note : must provide other elements except for id
// (created in this function)
const insertContact = async (contact) => {

  console.log("Inserting a new contact...");
  const query = {
    text: "INSERT INTO " + db.tableName + " (\"id\",\"firstname\",\"lastname\",\"email\",\"phone\", \"address\",\"notes\") VALUES ($1,$2,$3,$4,$5,$6,$7)",
    values: [contact.id, contact.firstname,contact.lastname,contact.email, contact.phone, contact.address, contact.notes],
  };

  // Debugging
  // console.log(query);

  const result = await db.executeQuery(query);
  return result;
};

const findByID = async (id) => {
  // Debugging
  // console.log(`Requesting a contact with id: ${id}...`);
  const result = await db.executeQuery(
    "SELECT * FROM " + db.tableName + "  WHERE id = $1;", [id]
  );

  // Debugging
  // console.log(`Found ${result.rowCount} contact(s).`);
  if (result.rowCount > 1) {
    const err = new Error("Internal error : Found multiple contacts.");
    err.name = "dbError";
    throw err;
  } else {
    return result;
  }   

};

const deleteByID = async (id) => {
  console.log(`Deleting product with id: ${id}`);
  const result = await db.executeQuery(
    "DELETE FROM " + db.tableName + " WHERE id = $1;", [id]
  );
  return result;
};

const updateByID = async (id, contact) => {
  const params = [id, contact.firstname,contact.lastname,contact.email, contact.phone, contact.address, contact.notes];
  console.log(`Updating a contact with ID ${id}...`);
  const result = await db.executeQuery(
    "UPDATE " + db.tableName + " SET firstname=$2, lastname=$3,email=$4, phone=$5, address=$6, notes=$7 WHERE id=$1;", 
    params
  );
  console.log(`At updateByID, result.rowCount is ${result.rowCount}.`);
  if (result.rowCount === 0) {
    console.log(`The contact with ID ${id} was not found.`);
  } else {
    console.log(`The contact with ID ${id} updated successfully.`);
  }
  return result;
};

export default {findAll, insertContact, findByID, deleteByID, updateByID};
