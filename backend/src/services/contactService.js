import contactDao from "../dao/contactDao.js";
import db from "../db/db.js";

// a valid contact information should have at least a first name and a last name
const validateContact = (contact) => {
  return contact["id"] && contact["firstname"] && contact["lastname"];
};

const findAll = async () => {
  const contacts = await contactDao.findAll();
  return contacts.rows;
};

const findByID = async (id) => {
  const contact = await contactDao.findByID(id);
  return contact;
};

const insertContact = async (contact) => {
  if (!validateContact(contact)) {
    const err = new Error("Incomplete contact information provided");
    err.name = "UserError";
    throw err;
  } else {

    // if some elements in contact information are missing => define them as null-strings
    // contactDao.insertContact expects each field to be defined
    db.tableFields.forEach(element => {
      if (contact[element] === undefined) {
        contact[element] = "";
      }
    });

    return await contactDao.insertContact(contact);
  }
};

const updateByID = async (reqID, contact) => {

  let result = 0; // default state : error

  if (!validateContact(contact)) {
    const err = new Error("Incomplete contact information provided");
    err.name = "UserError";
    throw err;
  } else {  

    // if some elements in contact information are missing => define them as null-strings
    // contactDao.updateContact expects each field to be defined
    db.tableFields.forEach(element => {
      if (contact[element] === undefined) {
        contact[element] = "";
      }
    });
    result = await contactDao.updateByID(reqID,contact);
  } 
    
  return result;
};

const deleteByID = async (reqID) => {

  const result = await contactDao.deleteByID(reqID);
  // Debugging:
  //   console.log("At service : result is.");
  //   console.log(result);
  return result;
};

export default { 
  findAll, insertContact,findByID, updateByID, deleteByID
};