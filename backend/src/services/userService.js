import jwt from "jsonwebtoken";
import bcrypt from "bcrypt";
import userDao from "../dao/userDao.js";

export const createUser = async (user) => {
  const saltRounds = 10;
  const passhash = await bcrypt.hash(user.password, saltRounds);
  const storableUser = {
    username: user.username,
    passhash
  };
  const result = await userDao.insertUser(storableUser);
  return result;
};

export const login = async (user) => {

  // Debugging
  // console.log("At userService.js");
  // console.log(`user.username = ${user.username}.`);
  // console.log(`user.password = ${user.password}.`);

  const userFromDb = await userDao.findUser(user.username);
  // Debugging
  // console.log("Search result from database:");
  // console.log(userFromDb);

  const passwordCorrect = userFromDb ? await bcrypt.compare(user.password, userFromDb.passhash) : false;

  // Debugging
  // console.log(`passwordCorrect = ${passwordCorrect}.`);

  if (!user || !passwordCorrect) {
    const error = new Error("Wrong username or password");
    error.name = "AuthError";
    throw error;
  }
  const token = jwt.sign(
    { 
      id: user.id,
      username: user.username
    },
    process.env.APP_SECRET,
    { expiresIn: 60*60 }
  );
  // Debugging
  // console.log(`token = ${token}.`);

  return token;
};