/* eslint-disable */

describe("### CREATING NEW CONTACT", () => {

    const contact = {
        firstname: 'Kiira',
        lastname: 'Koodari',
        email: 'mail@iki.fi',
        phone: '050 12341234',
        address: 'My magical street, 12345, Nicity',
        notes: 'Master coder'

    }
    beforeEach(() => {
        cy.visit('localhost:8080')
    })

    it("...with only first and lastname filled", () => {
        cy.contains('Add contact').click()

        cy.get('#text-field-first-name').focus()
            .type(contact.firstname)
            .should('have.value', contact.firstname)
            .wait(500)
        cy.get('#text-field-last-name').focus()
            .type(contact.lastname)
            .should('have.value', contact.lastname)
            .wait(500)
        cy.contains('Save').click()
        cy.get('th').last()
            .find('div')
            .should(($div) => {
                expect($div).to.contain(`${contact.firstname} ${contact.lastname}`)
            })
            .wait(500)
        cy.get('th').last().find('div').click()

        cy.get('th').last()
            .find('div>div').last().children()
            .should(($p) => {

                // expect($strong).to.contain(`Phone:`)
                expect($p, '4 items').to.have.length(8)
                expect($p.eq(0), 'first item').to.contain('Phone:')
                expect($p.eq(1), 'first item').to.contain('')
                expect($p.eq(2), 'second item').to.contain('Mail:')
                expect($p.eq(3), 'second item').to.contain('')
                expect($p.eq(4), 'third item').to.contain('Address:')
                expect($p.eq(5), 'third item').to.contain('')
                expect($p.eq(6), 'fourth item').to.contain('Notes:')
                expect($p.eq(7), 'fourth item').to.contain('')
            })
    })

    it("...with all fields filled", () => {
        cy.contains('Add contact').click()
            .wait(500)
        cy.get('#text-field-first-name').focus()
            .type(contact.firstname)
            .should('have.value', contact.firstname)
            .wait(500)

        cy.get('#text-field-last-name').focus()
            .type(contact.lastname)
            .should('have.value', contact.lastname)
            .wait(500)

        cy.get('#text-field-email').focus()
            .type(contact.email)
            .should('have.value', contact.email)

        cy.get('#text-field-phone').focus()
            .type(contact.phone)
            .should('have.value', contact.phone)
            .wait(500)

        cy.get('#text-field-address').focus()
            .type(contact.address)
            .should('have.value', contact.address)
            .wait(500)

        cy.get('#text-field-notes').focus()
            .type(contact.notes)
            .should('have.value', contact.notes)
            .wait(500)

        cy.contains('Save').click()
            .wait(500)

        cy.get('th').last()
            .find('div')
            .should(($div) => {
                expect($div).to.contain(`${contact.firstname} ${contact.lastname}`)
            })
            .wait(500)

        cy.get('th').last()
            .find('div').click()
            .wait(500)

        cy.get('th').last()
            .find('div>div').last()
            .children()
            .should(($p) => {
                expect($p, '4 items').to.have.length(8)
                expect($p.eq(0), 'first item').to.contain('Phone:')
                expect($p.eq(1), 'first item').to.contain(contact.phone)
                expect($p.eq(2), 'second item').to.contain('Mail:')
                expect($p.eq(3), 'second item').to.contain(contact.email)
                expect($p.eq(4), 'third item').to.contain('Address:')
                expect($p.eq(5), 'third item').to.contain(contact.address)
                expect($p.eq(6), 'fourth item').to.contain('Notes:')
                expect($p.eq(7), 'fourth item').to.contain(contact.notes)
            })
    })
})