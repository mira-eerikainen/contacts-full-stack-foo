import {
  Routes,
  BrowserRouter as Router,
  Route
} from "react-router-dom";

import "./App.css";
import { Container, CssBaseline } from "@mui/material";
import Header from "./components/Header";
import Contacts from "./pages/Contacts";

const App = () => {

  return (
    <Container
      maxWidth="false"
      disableGutters>
      <Router>
        <CssBaseline>
          <Header />
          <Routes>
            <Route
              exact path="/"
              element={<Contacts/>}>
            </Route>
          </Routes>
        </CssBaseline>
      </Router>
    </Container>
  );
};

export default App;

