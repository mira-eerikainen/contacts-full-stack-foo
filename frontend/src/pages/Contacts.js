/* eslint-disable */
import { Box, Container, TextField } from "@mui/material";
import { useState, useEffect } from "react";
import contactService from "../services/contactService";
import DataTable from "../components/DataGrid";
import ButtonContainer from "../components/ButtonContainer";
import AddContact from "../components/AddContactForm";

const Contacts = () => {
  const [contacts, setContacts] = useState([]);
  const [filterContacts, setFilterContacts] = useState([]);
  const [showAddForm, setShowAddForm] = useState(false);

  useEffect(() => {
    const getContacts = async () => {
      const { data } = await contactService.getAll();
      setContacts(data);
      setFilterContacts(data);
    };
    getContacts();
  }, []);

  const handleSearchChange = (e) => {
    e.preventDefault();
    const searchValue = e.target.value;
    const searchRegex = new RegExp(e.target.value, "ig");
    if (searchValue === "") {
      setFilterContacts(contacts);
    } else {
      const filtered = contacts.filter(contact =>
        Object.entries(contact).some(value => {
          if (value[0] !== "id") {
            return searchRegex.test(value[1]);
          }
        })
      );
      setFilterContacts(filtered);
    }
  };

  const handleAddContact = async (contactdetails) => {
    await contactService.create(contactdetails)
    const newList = [...contacts, contactdetails]
    newList.sort(function (a, b) {
      const nameA = a.firstname.toUpperCase();
      const nameB = b.firstname.toUpperCase();
      if (nameA < nameB) {
        return -1;
      }
      if (nameA > nameB) {
        return 1;
      }
      return 0;
    });
    setContacts(newList);
    setFilterContacts(newList);
    setShowAddForm(false);
  }
  // console.log(contacts);


  return (
    <>
    <Container
        maxWidth="sm"
        sx={{ margin: "36px auto 0 auto"}}>
        <TextField
          onChange={handleSearchChange}
          label="Search"
          variant="outlined"
          color="secondary"
          id={"text-field-search"}
          fullWidth
          required
        />
      </Container>
      <ButtonContainer
        onAdd={() => setShowAddForm(!showAddForm)}
        showAddForm={showAddForm}
      />
      <>
        {(showAddForm) && <AddContact onAdd={handleAddContact} />}
      </>
      <Box>
        <DataTable
          contacts={contacts}
          setContacts={setContacts}
          setFilterContacts={setFilterContacts}
          filterContacts={filterContacts}
        />
      </Box>
    </>
  );
};

export default Contacts;