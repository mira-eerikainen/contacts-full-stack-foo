import { AppBar, Typography } from "@mui/material";

const Header = () => {
  return (
    <AppBar position="static">
      <Typography
        p="12px"
        component="h1"
        variant="h4"
        align="center">
        Contacts
      </Typography>
    </AppBar>
  );
};

export default Header;