import Button from "@mui/material/Button";

const Btn = ( { text, color, onClick }) => {
  return (
    <Button
      color={color}
      onClick={onClick}
      type={text}
      variant="contained"
      sx={{ margin: "36px", width: "200px" }}>
      {text}
    </Button>
  );
};

export default Btn;