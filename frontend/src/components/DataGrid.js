import {
  Table,
  TableCell,
  TableContainer,
  TableBody,
  TableRow,
  Paper,
  Container,
  Button,
  Box,
} from "@mui/material";
import { useState } from "react";
import contactService from "../services/contactService";
import AddContact from "./AddContactForm";
import EditTwoToneIcon from "@mui/icons-material/EditTwoTone";
import DeleteTwoToneIcon from "@mui/icons-material/DeleteTwoTone";
import CloseIcon from "@mui/icons-material/Close";


export default function DataTable({ contacts, filterContacts, setContacts, setFilterContacts }) {
  // console.log(contacts);
  const [rowSelected, setRowSelected] = useState("");
  const [editMode, setEditMode] = useState("");

  const toggleRow = (id) => {
    if (!rowSelected) setRowSelected(id);
    else setRowSelected("");
  };

  const handleStartModify = (e, row) => {
    e.preventDefault();
    setEditMode(row.id);
  };

  const modifyContact = async (id, updatedDetails) => {
    console.log("modifyContact()", updatedDetails);
    await contactService.update(id, updatedDetails);
    setContacts(contacts.map((contact) => {
      if (id === contact.id) {
        contact = updatedDetails;
      }
      return contact;
    }));
    setFilterContacts(filterContacts.map((contact) => {
      if (id === contact.id) {
        contact = updatedDetails;
      }
      return contact;
    }));
    setEditMode("");
  };

  const handleRemove = async (e, id) => {
    e.preventDefault();
    console.log(`removing ${id}`);
    await contactService.remove(id);
    const newList = contacts.filter(c => c.id !== id);
    setContacts(newList);
    setFilterContacts(newList);
  };

  return (
    <Container maxWidth="sm">
      <TableContainer component={Paper}>
        <Table aria-label="simple table">
          <TableBody>
            {filterContacts.map((row, index) => (
              <TableRow
                key={`${row.name}${index}`}
                sx={{ "&:last-child td, &:last-child th": { border: 0 } }}
              >
                <TableCell component="th" scope="row">
                  {(rowSelected !== row.id) ?
                    <Box
                      onClick={() => toggleRow(row.id)}
                      id="full-name-clickable"
                    >
                      {row.firstname} {row.lastname}
                    </Box>
                    :
                    <div>
                      <div>
                        <div 
                          className="full-name"> 
                          {row.firstname} {row.lastname} 
                        </div>
                        {
                          (editMode !== row.id) ?
                            <div onClick={() => toggleRow(row.id)}>
                              <strong>Phone:</strong>
                              <p className="info-row">{row.phone}</p>
                              <strong>Email:</strong>
                              <p className="info-row">{row.email}</p>
                              <strong>Address:</strong>
                              <p className="info-row"> {row.address}</p>
                              <strong>Notes:</strong>
                              <p className="info-row"> {row.notes}</p>
                            </div>
                            :
                            <AddContact modifyDetails={row} onModify={modifyContact} />
                        }
                      </div>
                      <div>
                        {
                          (editMode !== row.id) ?
                            <Box sx={{ display: "flex", justifyContent: "end", margin: "24px 0 12px 0" }}>
                              <Button
                                onClick={(e) => handleStartModify(e, row)}
                                sx={{ padding: 0 }}>
                                <EditTwoToneIcon />
                              </Button>
                              <Button
                                onClick={(e) => handleRemove(e, row.id)}
                                sx={{ padding: 0 }}>
                                <DeleteTwoToneIcon />
                              </Button>
                              <Button
                                onClick={() => toggleRow(row.id)}
                                sx={{ padding: 0 }}>
                                <CloseIcon />
                              </Button>
                            </Box>
                            :
                            <div></div>
                        }
                      </div>
                    </div>
                  }
                </TableCell>
              </TableRow>
            ))}
          </TableBody>
        </Table>
      </TableContainer>
    </Container>
  );
}