import { useState } from "react";
import { Box, Container, TextField } from "@mui/material";
import { v4 as uuidv4 } from "uuid";

import Btn from "./Button";

const AddContact = ({ onAdd, modifyDetails, onModify }) => {
  const [firstName, setFirstName] = useState("");
  const [lastName, setLastName] = useState("");
  const [email, setEmail] = useState("");
  const [phoneNumber, setPhoneNumber] = useState("");
  const [address, setAddress] = useState("");
  const [notes, setNotes] = useState("");
  // console.log("modifyDetails", modifyDetails);

  const handleSubmit = (e) => {
    e.preventDefault();

    let newContact = {
      id: uuidv4(),
      firstname: firstName,
      lastname: lastName,
      email: email,
      phone: phoneNumber,
      address: address,
      notes: notes
    };

    if (modifyDetails) {
      newContact = {
        id: modifyDetails.id,
        firstname: firstName ? firstName : modifyDetails.firstname,
        lastname: lastName ? lastName : modifyDetails.lastname,
        email: email ? email : modifyDetails.email,
        phone: phoneNumber ? phoneNumber : modifyDetails.phonenumber,
        address: address ? address : modifyDetails.address,
        notes: notes ? notes : modifyDetails.notes
      };
    }

    if (modifyDetails) {
      onModify(modifyDetails.id, newContact);
    } else {
      onAdd(newContact);
    }

    setFirstName("");
    setLastName("");
    setEmail("");
    setPhoneNumber("");
    setAddress("");
    setNotes("");
  };

  return (
    <Container
      maxWidth="sm">
      <Box
        component="form"
        onSubmit={handleSubmit}
        autoComplete="off">
        <TextField
          margin="dense"
          onChange={(e) => setFirstName(e.target.value)}
          multiline
          maxRows={2}
          label="First Name"
          defaultValue={!modifyDetails ?
            ""
            :
            modifyDetails.firstname}
          variant="outlined"
          color="secondary"
          id={"text-field-first-name"}
          fullWidth
          required
        />
        <TextField
          margin="dense"
          onChange={(e) => setLastName(e.target.value)}
          multiline
          maxRows={2}
          label="Last Name"
          defaultValue={!modifyDetails ?
            ""
            :
            modifyDetails.lastname}
          variant="outlined"
          color="secondary"
          id={"text-field-last-name"}
          fullWidth
          required
        />
        <TextField
          margin="dense"
          onChange={(e) => setEmail(e.target.value)}
          multiline
          maxRows={2}
          label="Email Address"
          defaultValue={!modifyDetails ?
            ""
            :
            modifyDetails.email}
          type="email"
          variant="outlined"
          color="secondary"
          id={"text-field-email"}
          fullWidth
        />
        <TextField
          margin="dense"
          onChange={(e) => setPhoneNumber(e.target.value)}
          label="Phone Number"
          defaultValue={!modifyDetails ?
            ""
            :
            modifyDetails.phone}
          variant="outlined"
          color="secondary"
          id={"text-field-phone"}
          fullWidth
        />
        <TextField
          margin="dense"
          onChange={(e) => setAddress(e.target.value)}
          multiline
          maxRows={2}
          label="Street Address"
          defaultValue={!modifyDetails ?
            ""
            :
            modifyDetails.address}
          variant="outlined"
          color="secondary"
          id={"text-field-address"}
          fullWidth
        />
        <TextField
          margin="dense"
          onChange={(e) => setNotes(e.target.value)}
          multiline
          maxRows={2}
          label="Notes"
          defaultValue={!modifyDetails ?
            ""
            :
            modifyDetails.notes}
          variant="outlined"
          color="secondary"
          id={"text-field-notes"}
          fullWidth
        />

        {!modifyDetails ?
          <Btn
            text={"Save"}
            type={"submit"}
            id={"btn-submit-add"}
          >
          </Btn>
          :
          <Container sx={{ textAlign: "center" }}>
            <Btn
              text={"Update"}
              type={"submit"}
              id={"btn-submit-add"}
            >
            </Btn>
          </Container>
          
        }
      </Box>
    </Container>
  );
};

export default AddContact;