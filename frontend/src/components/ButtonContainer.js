import { Container } from "@mui/material";
import Btn from "./Button";

const ButtonContainer = ({ onAdd, showAddForm }) => {
  return (
    <Container
      maxWidth="sm">
      <Btn
        onClick={onAdd}
        text={showAddForm ? "Cancel" : "Add contact"}
        color={showAddForm ? "secondary" : "primary"}
        id={"btn-add-contact"}
      >
      </Btn>
    </Container>
  );
};

export default ButtonContainer;