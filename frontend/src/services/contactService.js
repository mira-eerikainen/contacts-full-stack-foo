import axios from "axios";
const baseUrl = "/contacts";

const getAll = async () => {
  const result = await axios.get(baseUrl);
  return result;
};

const create = async (newObject) => {
  const result = await axios.post(baseUrl, newObject);
  return result;
};

const update = async (id, newObject) => {
  const result = await axios.put(`${baseUrl}/${id}`, newObject);
  return result;
};

const remove = async (id) => {
  const result = await axios.delete(`${baseUrl}/${id}`);
  return result;
};

const contactService = {
  getAll,
  create,
  update,
  remove
};

export default contactService;