# Contacts-full-stack-foo

Buutti Oy  
Training project

## Members:  
Mira Eerikäinen  
Rafael Lehmann  
Toni Nyberg  
Mikko Törnqvist

## Azure address:
https://contact-foo-app.azurewebsites.net

### Project details
Frontend implemented using React  
Backend (CRUD functionality) implemented using Express and PostgreSQL

### Configuration Details
Please refer to   
/frontend/README.md  
/backend/README.md   
for configuration details

